const mongoose = require('mongoose');

function dbConnection() {
  mongoose.connect('mongodb://localhost:27017/mean-project', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
      console.log('Connection to databse established!');
    })
    .catch(() => {
      console.log('Connection to databse failed!');
    });
}

module.exports = dbConnection;
