const router = require('express').Router();
const postController = require('../controllers/posts.controller');
const checkAuth = require('../middlewares/check-auth');
const extractFile = require('../middlewares/file');



router.post('', checkAuth, extractFile, postController.createPost);

router.get('/:id', postController.getpostById);

router.get('', postController.getALlPosts);

router.put('/:id', checkAuth, extractFile, postController.updateOnePost);

router.delete('/:id', checkAuth, postController.deletePostById);

module.exports = { router };
