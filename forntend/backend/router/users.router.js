const router = require('express').Router();

const userController = require('../controllers/users.controller');

router.post('/signup', userController.userSignup);

router.post('/login', userController.userLogin);

module.exports = { router };
