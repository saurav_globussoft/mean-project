const Post = require('../models/post');

class PostController {

  async createPost(req, res) {
    try {
      const url = req.protocol + '://' + req.get('host');
      const post = new Post({
        title: req.body.title,
        content: req.body.content,
        imagePath: url + '/images/' + req.file.filename,
        creator: req.user.userId
      });

      const savedData = await post.save();
      res.status(201).json({
        message: 'Post added succesfully',
        post: {
          ...savedData,
          id: savedData._id
        }
      });
    } catch (error) {
      res.status(400).json({ message: 'Post creation Failed' });
    }
  }

  async getpostById(req, res) {
    try {
      const { id } = req.params;
      const post = await Post.findOne({ _id: id });
      if (post) {
        res.status(200).json({ post });
      } else {
        res.status(404).json({ message: 'Post not found' });
      }
    } catch (error) {
      res.status(400).json({ message: 'Fetching post failed' });
    }
  }

  async getALlPosts(req, res) {
    try {
      const pageSize = +req.query.pagesize;
      const currentPage = +req.query.page;
      const postQuery = Post.find();
      if (pageSize && currentPage) {
        postQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
      }

      const posts = await postQuery.find();

      const postCount = await Post.count();

      res.status(200).json({
        message: 'Posts fetched successfully',
        posts: posts,
        maxPosts: postCount
      });
    } catch (error) {
      res.status(400).json({ message: 'Fetching post failed' });
    }
  }

  async updateOnePost(req, res) {
    try {
      const { id } = req.params;
      let imagePath = req.body.imagePath;
      if (req.file) {
        const url = req.protocol + '://' + req.get('host');
        imagePath = url + '/images/' + req.file.filename;
      }
      const post = new Post({
        _id: req.body.id,
        title: req.body.title,
        content: req.body.content,
        imagePath: imagePath,
        creator: req.user.userId
      });

      const updatedData = await Post.updateOne({ _id: id, creator: req.user.userId }, post);
      if (updatedData.modifiedCount > 0) {
        res.status(200).json({
          message: 'Post updated succesfully'
        });
      } else {
        res.status(401).json({
          message: 'Unauthorised user'
        });
      }
    } catch (error) {
      res.status(400).json({ message: `Post couldn't updated` });
    }
  }

  async deletePostById(req, res) {
    try {
      const { id } = req.params;
      const deletedPost = await Post.deleteOne({ _id: id, creator: req.user.userId });
      if (deletedPost.deletedCount > 0) {
        res.status(200).json({
          message: 'Post deleted succesfully'
        });
      } else {
        res.status(401).json({
          message: 'Unauthorised user'
        });
      }
    } catch (error) {
      res.status(400).json({ message: `Post couldn't deleted` });
    }
  }
}

module.exports = new PostController;
