const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user');

class UserController {

  async userSignup(req, res) {
    try {
      const { email, password } = req.body;

      const hash = await bcrypt.hash(password, 10);
      const user = new User({
        email: email,
        password: hash
      });

      const savedData = await user.save();

      res.status(201).json({
        message: 'User added succesfully',
        user: savedData
      });
    } catch (error) {
      res.status(400).json({ message: 'Invalid authentication credentials' });
    }
  }

  async userLogin(req, res) {
    try {
      const { email, password } = req.body;

      const checkUserExist = await User.findOne({ email });

      if (!checkUserExist) {
        return res.status(401).json({ message: 'Invalid authentication credentials' });
      }

      const match = await bcrypt.compare(password, checkUserExist.password);

      if (!match) {
        return res.status(400).json({ message: 'Password Incorrect' });
      }

      const token = jwt.sign({
        email: checkUserExist.email,
        userId: checkUserExist._id
      }, 'some_new_secret_key', { expiresIn: 60 * 60 });

      res.status(200).json({
        token: token,
        expiresIn: 3600,
        userId: checkUserExist._id
       });
    } catch (error) {
      res.status(400).json({ error });
    }
  }
}

module.exports = new UserController;
