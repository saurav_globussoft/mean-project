const express = require('express');
const bodyParser = require('body-parser');
const dbConnection = require('./dbConnection/dbConnection');
const path = require('path');

const { router: postRouter } = require('./router/posts.router');

const { router: userRouter } = require('./router/users.router');

const app = express();

dbConnection();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/images', express.static(path.join('backend/images')));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PUT, PATCH, DELETE, OPTIONS'
  );
  next();
});

app.use('/api/posts', postRouter);

app.use('/api/user', userRouter);

// 202.142.118.32/32
// mxtyapCyjYSS9zL

module.exports = app;
