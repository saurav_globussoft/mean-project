import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { Post } from './post.model';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

const URL = environment.apiUrl + '/posts';
@Injectable({
  providedIn: 'root'
})
export class PostService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<{ posts: Post[], postCount: number }>();

  constructor(private http: HttpClient, private router: Router) { }

  getPosts(postsPerPage: number, currentPage: number) {
    const queryParams = `?pagesize=${postsPerPage}&page=${currentPage}`;
    this.http.get<{ message: string, posts: any, maxPosts: number }>(URL + queryParams)
      .pipe(map((res) => {
        return {
          posts: res.posts.map((post: any) => {
            return {
              title: post.title,
              content: post.content,
              id: post._id,
              imagePath: post.imagePath,
              creator: post.creator
            };
          }),
          maxPosts: res.maxPosts
        };
      }))
      .subscribe((dbPosts) => {
        this.posts = dbPosts.posts;
        this.postsUpdated.next({
          posts: [...this.posts],
          postCount: dbPosts.maxPosts
        });
      });
  }

  getSinglePost(id: string): Observable<any> {
    return this.http.get(URL + '/' + id);
  }

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }

  addPosts(title: string, content: string, image: File) {
    const postData = new FormData();
    postData.append('title', title);
    postData.append('content', content);
    postData.append('image', image, title);

    this.http.post<{ message: string, post: Post }>(URL, postData)
      .subscribe((res) => {
        this.router.navigate(["/"]);
      });
  }

  updatePost(id: string, title: string, content: string, image: File | string) {
    let postData: Post | FormData;

    if (typeof image === 'object') {
      postData = new FormData();
      postData.append('id', id);
      postData.append('title', title);
      postData.append('content', content);
      postData.append('image', image, title);
    } else {
      postData = {
        id: id,
        title: title,
        content: content,
        imagePath: image,
        creator: ''
      };
    }
    this.http.put<{ message: string, postId: string }>(URL + '/' + id, postData)
      .subscribe((res) => {
        this.router.navigate(["/"]);
      });
  }

  deletePosts(postId: string) {
    return this.http.delete(URL + '/' +postId);
  }
}
