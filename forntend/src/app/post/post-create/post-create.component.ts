import { AuthService } from 'src/app/auth/auth.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { PostService } from '../post.service';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit, OnDestroy {
  private mode = 'create';
  private postId: any;
  post: any;
  isLoading: boolean = false;
  imagePreview: string = '';
  private authStatusSub!: Subscription;

  constructor(public postService: PostService, public route: ActivatedRoute, private authService: AuthService) { }

  ngOnInit(): void {
    this.authStatusSub = this.authService.getAuthStatusListener()
      .subscribe(authStatus => {
        this.isLoading = false;
      })
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('postId')) {
        this.mode = 'edit';
        this.postId = paramMap.get('postId');
        this.isLoading = true;
        this.postService.getSinglePost(this.postId).subscribe(res => {
          this.isLoading = false;
          this.post = { id: res.post._id, title: res.post.title, content: res.post.content, imagePath: '' };
          this.postForm.patchValue({
            title: this.post.title,
            content: this.post.content,
            image: this.post.imagePath
          });
        });
      } else {
        this.mode = 'create';
        this.postId = '';
      }
    });
  }

  postForm = new FormGroup({
    title: new FormControl('', { validators: [Validators.required, Validators.minLength(3)] }),
    content: new FormControl('', { validators: [Validators.required, Validators.minLength(10)] }),
    image: new FormControl('', Validators.required)
  });

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files![0];
    this.postForm.patchValue({ image: file });
    this.postForm.get('image')?.updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    }
    reader.readAsDataURL(file);
  }

  onSavePost() {
    if (!this.postForm.valid) {
      return;
    }
    this.isLoading = true;
    if (this.mode == 'create') {
      this.postService.addPosts(this.postForm.value.title, this.postForm.value.content, this.postForm.value.image);
    } else {
      this.postService.updatePost(this.postId, this.postForm.value.title, this.postForm.value.content, this.postForm.value.image);
    }

    this.postForm.reset();
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }

}
